import { Component } from '@angular/core';
import { PopupService } from './common/service/popup_service/popup.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private popupService: PopupService) {
  }

}