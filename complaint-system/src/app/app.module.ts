import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './common/component/header/header.component';
import { LoginComponent } from './login/component/login/login.component';
import { FooterComponent } from './common/component/footer/footer.component';
import { PageNotFoundComponent } from './common/component/page-not-found/page-not-found.component';
import { AdminComponent } from './admin/component/admin/admin.component';
import { ManageAreaComponent } from './admin/component/manage-area/manage-area.component';
import { ManageCategoriesComponent } from './admin/component/manage-categories/manage-categories.component';
import { ManageComplaintComponent } from './admin/component/manage-complaint/manage-complaint.component';
import { ManageEmployeeComponent } from './admin/component/manage-employee/manage-employee.component';
import { ViewComplaintComponent } from './complaint/component/view-complaint/view-complaint.component';
import { EmployeeComponent } from './employee/component/employee/employee.component';
import { ManageComplaintStatusComponent } from './employee/component/manage-complaint-status/manage-complaint-status.component';
import { UserComponent } from './user/component/user/user.component';
import { UserProfileComponent } from './user/component/user-profile/user-profile.component';
import { SignupComponent } from './user/component/signup/signup.component';
import { RegisterComplaintComponent } from './user/component/register-complaint/register-complaint.component';
import { PastComplaintDetailComponent } from './user/component/past-complaint-detail/past-complaint-detail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { NavbarComponent } from './common/component/navbar/navbar.component';
import { HomeComponent } from './common/component/home/home.component';
import { ChangePasswordComponent } from './common/component/change-password/change-password.component';
import { ManageUsersComponent } from './admin/component/manage-users/manage-users.component';
import { PopupComponent } from './common/component/popup/popup/popup.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    FooterComponent,
    PageNotFoundComponent,
    AdminComponent,
    ManageAreaComponent,
    ManageCategoriesComponent,
    ManageComplaintComponent,
    ManageEmployeeComponent,
    ViewComplaintComponent,
    EmployeeComponent,
    ManageComplaintStatusComponent,
    UserComponent,
    UserProfileComponent,
    SignupComponent,
    RegisterComplaintComponent,
    PastComplaintDetailComponent,
    NavbarComponent,
    HomeComponent,
    ChangePasswordComponent,
    ManageUsersComponent,
    PopupComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
