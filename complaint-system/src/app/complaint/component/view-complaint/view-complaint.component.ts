import { Component, OnInit, Input } from '@angular/core';
import { ComplainDetailModel } from '../../../common/model/complain_detail_model/complain-detail-model';
import { ComplainDetailService } from '../../../common/service/complain_detail_service/complain-detail.service';
import { CommentService } from '../../../common/service/comments_service/comment.service';
import { CommentModel } from 'src/app/common/model/comment_model/comment-model';
import { AreaService } from '../../../common/service/area_service/area.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from '../../../common/service/category_service/category.service';
import { LoginStateModel } from 'src/app/common/model/loginstate_model/login-state-model';
import { LoginStateService } from 'src/app/common/service/login_state/loginstate.service';
import { ComplaintService } from '../../../common/service/complaint_service/complaint.service';
import { EmployeeService } from 'src/app/common/service/employee_service/employee.service';
import { UserService } from 'src/app/common/service/user_service/user.service';
import { AppComponent } from '../../../app.component';

@Component({
  selector: 'app-view-complaint',
  templateUrl: './view-complaint.component.html',
  styleUrls: ['./view-complaint.component.css']
})
export class ViewComplaintComponent implements OnInit {
  commentForm: FormGroup;
  isHide: boolean;
  complainDetailModel: ComplainDetailModel[];
  viewAllcomplainDetailModal: ComplainDetailModel;
  compId: number;
  submitted = false;
  complainForm: any;
  compDate: string;
  loginState: LoginStateModel;
  options: string[];
  listOfComplaint = 10;

  constructor(
    private formBuilder: FormBuilder,
    private complainDetailService: ComplainDetailService,
    private commentService: CommentService,
    private loginStateService: LoginStateService,
    private areaService: AreaService,
    private categoryService: CategoryService,
    private employeeService: EmployeeService,
    private userService: UserService,
    private complainService: ComplaintService,
  ) {

    if (window.location.href.includes('/pastcomplaint')) {
      this.complainDetailModel = this.complainDetailService.getComplainDetailsByUserId(this.loginStateService.loginState.id);
    } else {
      this.complainDetailModel = this.complainDetailService.getLatestComplainDetails(this.listOfComplaint);
    }

    this.options = this.areaService.getAreaNames();
  }

  ngOnInit() {
    this.isHide = true;
    this.commentForm = this.formBuilder.group({
      comment: ['', [Validators.required, Validators.maxLength(50)]],
    });
    this.options = this.areaService.getAreaNames();
  }

  get formfields() {
    return this.commentForm.controls;
  }
  dueDate(date: string): number {
    const date1 = new Date();
    const date2 = new Date(date);
    const differenceInTime = date1.getTime() - date2.getTime();
    return Math.floor(differenceInTime / (1000 * 3600 * 24));
  }

  viewModal(comp: ComplainDetailModel) {
    this.isHide = !this.isHide;
    this.viewAllcomplainDetailModal = comp;
    this.viewAllcomplainDetailModal.comments = this.commentService.getCommentsDetailByComplainId(this.viewAllcomplainDetailModal.compId);
  }
  closeModal() { this.isHide = !this.isHide; }


  onSubmit() {
    if (this.commentForm.invalid) {
      this.submitted = true;
      return;
    }
    const comment: CommentModel = {
      commentId: this.commentService.getLastCommentId(),
      userId: this.loginStateService.loginState.id,
      complainId: this.viewAllcomplainDetailModal.compId,
      comment: this.formfields.comment.value
    };
    this.commentService.addNewComments(comment);
    this.viewAllcomplainDetailModal.comments = this.commentService.getCommentsDetailByComplainId(this.viewAllcomplainDetailModal.compId);
    this.submitted = false;
    this.commentForm.reset();
  }


  categoryChanged(val) {
    if (val === 'Area') {
      this.options = this.areaService.getAreaNames();
    } else if (val === 'Category') {
      this.options = this.categoryService.getCategoryNames();
    } else if (val === 'Employee') {
      this.options = this.employeeService.getAllEmployeeNameAndId();
    } else if (val === 'User') {
      this.options = this.userService.getAllUsersNameAndId();
    } else {
      this.options = this.complainService.getAllStatusTypes();
    }
  }

  search(combobox1, combobox2) {
    const id = parseInt(combobox2.split('-')[0], 10);
    if (combobox1 === 'Area') {
      this.complainDetailModel = this.complainDetailService
        .getComplainDetailsByAreaId(this.areaService.getAreaIdByName(combobox2));
    } else if (combobox1 === 'Category') {
      this.complainDetailModel = this.complainDetailService
        .getComplainDetailsByCategoryId(this.categoryService.getCategoryIdByName(combobox2));
    } else if (combobox1 === 'Employee') {
      this.complainDetailModel = this.complainDetailService
        .getComplainDetailsByEmployeeId(id);
    } else if (combobox1 === 'User') {
      this.complainDetailModel = this.complainDetailService
        .getComplainDetailsByUserId(id);
    } else if (combobox1 === 'Status') {
      this.complainDetailModel = this.complainDetailService
        .getComplainDetailsByStatus(combobox2);
    }

  }

  getCategories(): string[] {
    const options: string[] = ['Area', 'Category'];
    if (this.loginStateService.loginState && this.loginStateService.loginState.role === 'Admin') {
      options.push('User', 'Employee', 'Status');
    }
    return options;
  }
}
