import { Component, OnInit } from '@angular/core';
import { EmployeeModel } from '../../../common/model/employee_model/employee-model';
import { EmployeeService } from 'src/app/common/service/employee_service/employee.service';
import { Router } from '@angular/router';
import { LoginStateService } from 'src/app/common/service/login_state/loginstate.service';
import { ComplainDetailModel } from '../../../common/model/complain_detail_model/complain-detail-model';
import { ComplainDetailService } from '../../../common/service/complain_detail_service/complain-detail.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  employees: EmployeeModel[];
  complainDetailModel: ComplainDetailModel[];
  constructor(
    private employeeService: EmployeeService,
    public router: Router,
    private loginStateService: LoginStateService,
    private complainDetailService: ComplainDetailService) {
    if (this.loginStateService.isLoginStateValidOfUser('Employee')) {
      this.router.navigate(['login']);
      return;
    }
    this.complainDetailModel = complainDetailService.getComplainDetailsByEmployeeId(this.loginStateService.loginState.id);
    this.employees = this.employeeService.getAllDataFromLocalStorage();
  }
  ngOnInit() {
  }

  viewComplaintDetail(compId) {
    this.router.navigate(['managecomplaintstatus', compId]);
  }

}
