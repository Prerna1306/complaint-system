import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageComplaintStatusComponent } from './manage-complaint-status.component';

describe('ManageComplaintStatusComponent', () => {
  let component: ManageComplaintStatusComponent;
  let fixture: ComponentFixture<ManageComplaintStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageComplaintStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageComplaintStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
