import { Component, OnInit } from '@angular/core';
import { LoginStateModel } from 'src/app/common/model/loginstate_model/login-state-model';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginStateService } from 'src/app/common/service/login_state/loginstate.service';
import { ComplainDetailModel } from '../../../common/model/complain_detail_model/complain-detail-model';
import { ComplainDetailService } from '../../../common/service/complain_detail_service/complain-detail.service';
import { SolutionService } from 'src/app/common/service/solution_servie/solution.service';
import { SolutionModel } from '../../../common/model/solution_model/solution-model';
import { ComplaintService } from 'src/app/common/service/complaint_service/complaint.service';
import { CommentService } from 'src/app/common/service/comments_service/comment.service';

@Component({
  selector: 'app-manage-complaint-status',
  templateUrl: './manage-complaint-status.component.html',
  styleUrls: ['./manage-complaint-status.component.css']
})

export class ManageComplaintStatusComponent implements OnInit {

  compId: number;
  complainDetailModel: ComplainDetailModel;
  solutionError = false;

  constructor(
    public router: Router,
    private loginStateService: LoginStateService,
    private activatedRoute: ActivatedRoute,
    private complainDetailService: ComplainDetailService,
    private complaintService: ComplaintService,
    private solutionService: SolutionService,
    private commentService: CommentService,
  ) {
    if (this.loginStateService.isLoginStateValidOfUser('Employee')) {
      this.router.navigate(['login']);
      return;
    }
    this.compId = +this.activatedRoute.snapshot.paramMap.get('id');
    this.complainDetailModel = this.complainDetailService.getComplainDetailsByComplainId(this.compId);
    this.complainDetailModel.comments = this.commentService.getCommentsDetailByComplainId(this.compId);
  }

  ngOnInit() {
  }

  submitSolution(status: string, solution: string) {
    if (status === 'Solved') {
      if (solution.trim().length === 0) {
        this.solutionError = true;
        return;
      } else {
        this.solutionError = false;
        const solutionObj: SolutionModel = {
          solutionId: this.solutionService.getLastSolutionId(),
          empId: this.complainDetailModel.employee.empId,
          complainId: this.complainDetailModel.compId,
          solution
        };
        this.solutionService.addNewSolution(solutionObj);
        this.complaintService.updateStatusOfCompliainByComplainId(this.complainDetailModel.compId, 'Solved');
        this.complaintService.updateCompletetDateOfCompliainByComplainId(this.complainDetailModel.compId);
        this.router.navigate(['/employee']);
      }
    } else {
      const solutionObj: SolutionModel = {
        solutionId: this.solutionService.getLastSolutionId(),
        empId: this.complainDetailModel.employee.empId,
        complainId: this.complainDetailModel.compId,
        solution: 'This complain is marked as fake!'
      };
      this.solutionService.addNewSolution(solutionObj);
      this.complaintService.updateStatusOfCompliainByComplainId(this.complainDetailModel.compId, 'Fake');
      this.complaintService.updateCompletetDateOfCompliainByComplainId(this.complainDetailModel.compId);
      this.router.navigate(['/employee']);
    }
  }

  onCancel() {
    this.router.navigate(['/employee']);
  }

}
