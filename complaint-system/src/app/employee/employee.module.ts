import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeComponent } from './component/employee/employee.component';



@NgModule({
  declarations: [EmployeeComponent],
  imports: [
    CommonModule
  ]
})
export class EmployeeModule { }
