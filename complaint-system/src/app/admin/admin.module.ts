import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './component/admin/admin.component';
import { RouterModule, Routes } from '@angular/router';
import { ManageAreaComponent } from './component/manage-area/manage-area.component';
import { ManageCategoriesComponent } from './component/manage-categories/manage-categories.component';



const routes: Routes = [
  { path: 'area', component: ManageAreaComponent },
  { path: 'category', component: ManageCategoriesComponent },
];
@NgModule({
  declarations: [AdminComponent],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ]
})
export class AdminModule { }
