import { Component, OnInit } from '@angular/core';
import { LoginStateService } from '../../../common/service/login_state/loginstate.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-manage-complaint',
  templateUrl: './manage-complaint.component.html',
  styleUrls: ['./manage-complaint.component.css']
})
export class ManageComplaintComponent implements OnInit {
  constructor(
    private loginStateService: LoginStateService,
    private router: Router
  ) {
    if (this.loginStateService.isLoginStateValidOfUser('Admin')) {
      this.router.navigate(['login']);
      return;
    }
  }

  ngOnInit() {

  }

}
