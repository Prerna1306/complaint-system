import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CategoryService } from '../../../common/service/category_service/category.service';
import { CategoryModel } from 'src/app/common/model/category_model/category-model';
import { LoginStateService } from 'src/app/common/service/login_state/loginstate.service';
import { Router } from '@angular/router';
import { PopupService } from '../../../common/service/popup_service/popup.service';

@Component({
  selector: 'app-manage-categories',
  templateUrl: './manage-categories.component.html',
  styleUrls: ['./manage-categories.component.css']
})
export class ManageCategoriesComponent implements OnInit {
  categoryForm: FormGroup;
  submitted = false;
  categoryData: CategoryModel[];
  action = 'Add';
  selectedEmployeeId = 0;
  categoryNameExists = false;
  prevName = '';

  constructor(
    private formBuilder: FormBuilder,
    private catService: CategoryService,
    private loginStateService: LoginStateService,
    private categoryService: CategoryService,
    private router: Router,
    private popupService: PopupService) {
    this.categoryForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(50)]],
      description: ['', [Validators.required, Validators.maxLength(255)]],
      catId: [''],
      status: ['Active'],
    });
    if (this.loginStateService.isLoginStateValidOfUser('Admin')) {
      this.router.navigate(['login']);
      return;
    }
  }

  ngOnInit() {
    this.setCategoryData();
  }

  onSubmit() {
    if (this.categoryForm.invalid) {
      this.submitted = true;
      return;
    } else if (this.action === 'Add') {
      {
        this.categoryNameExists = this.categoryService.isCategoryUnique(this.formFields.name.value);
        if (this.categoryNameExists) {
          return;
        }
        const category: CategoryModel = {
          catId: this.catService.getLastCategoryId(),
          catName: this.formFields.name.value,
          description: this.formFields.description.value,
          status: 1
        };
        this.catService.addNewCategory(category);
        this.setCategoryData();
        this.categoryForm.reset();
        this.submitted = false;
        this.categoryNameExists = false;
        this.popupService.setSuccessMessage('Successfully Category Added !!');
        this.popupService.activateVisibleState();
      }
    } else if (this.action === 'Update') {
      if (this.prevName === this.formFields.name.value) {
        this.categoryNameExists = true;
        this.updateCategoryData();
      } else if (this.categoryService.isCategoryUnique(this.formFields.name.value)) {
        this.categoryNameExists = true;
      } else {
        this.categoryNameExists = true;
        this.updateCategoryData();
      }
    }
  }

  setCategoryData() {
    this.categoryData = JSON.parse(localStorage.getItem('CATEGORYDATA'));
  }

  get formFields() {
    return this.categoryForm.controls;
  }

  display(catD: CategoryModel) {
    this.categoryNameExists = false;
    this.action = 'Update';
    this.categoryForm.patchValue(
      {
        catId: catD.catId,
        name: catD.catName,
        description: catD.description,
        status: catD.status
      });
    this.prevName = catD.catName;
    this.selectedEmployeeId = catD.catId;
  }

  updateCategoryData() {
    this.selectedEmployeeId = 0;
    this.catService.updateDataToLocalStorage({
      catId: 0,
      catName: this.formFields.name.value,
      description: this.formFields.description.value,
      status: this.formFields.status.value === 'Active' ? 1 : 0
    }, this.formFields.catId.value);
    this.setCategoryData();
    this.submitted = false;
    this.categoryForm.reset();
    this.action = 'Add';
    this.categoryNameExists = false;
    this.popupService.setSuccessMessage('Successfully Category Updated !!');
    this.popupService.activateVisibleState();
  }
}
