import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmployeeModel } from 'src/app/common/model/employee_model/employee-model';
import { EmployeeService } from 'src/app/common/service/employee_service/employee.service';
import { LoginStateService } from '../../../common/service/login_state/loginstate.service';
import { Router } from '@angular/router';
import { PopupService } from '../../../common/service/popup_service/popup.service';

@Component({
  selector: 'app-manage-employee',
  templateUrl: './manage-employee.component.html',
  styleUrls: ['./manage-employee.component.css']
})
export class ManageEmployeeComponent implements OnInit {
  dataActive = 'Active';
  selectedEmpId = 0;
  formEmp: FormGroup;
  submitted = false;
  empdata: any;
  userStatus: string[];
  action = 'Add';
  constructor(
    private empService: EmployeeService,
    private formBuilder: FormBuilder,
    private loginStateService: LoginStateService,
    private popupService: PopupService,
    private router: Router) {
    if (this.loginStateService.isLoginStateValidOfUser('Admin')) {
      this.router.navigate(['login']);
      return;
    }
  }

  ngOnInit() {
    this.formEmp = this.formBuilder.group({
      empId: [this.getId()],
      firstName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
      lastName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
      userName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
      password: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
      email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      address: ['', Validators.required],
      empStatus: ['Active']
    });
  }

  displayEmp(empdata: EmployeeModel) {
    this.selectedEmpId = empdata.empId;
    this.action = 'Update';
    this.formEmp.setValue({
      empId: empdata['empId'],
      firstName: empdata['firstName'],
      lastName: empdata['lastName'],
      userName: empdata['userName'],
      password: empdata['password'],
      email: empdata['email'],
      address: empdata['address'],
      empStatus: empdata.status ? 'Active' : 'Inactive'
    });
  }

  getEmployeeDetails() {
    return this.empService.getAllDataFromLocalStorage();
  }

  get dataEmp() {
    return this.formEmp.controls;
  }

  getId(): number {
    return this.empService.getNextId();
  }

  onReset() {
    this.action = 'Add';
    this.formEmp.patchValue({
      empId: [this.getId()],
      firstName: '',
      lastName: '',
      userName: '',
      password: '',
      email: '',
      address: '',
      empStatus: true
    });
    this.selectedEmpId = 0;
  }

  onSave() {
    let checkResetdata = false;
    if (this.formEmp.invalid) {
      this.submitted = true;
      return;
    }
    const empModel: EmployeeModel = {
      empId: this.formEmp.controls.empId.value,
      firstName: this.formEmp.controls.firstName.value,
      lastName: this.formEmp.controls.lastName.value,
      userName: this.formEmp.controls.userName.value,
      password: this.formEmp.controls.password.value,
      email: this.formEmp.controls.email.value,
      address: this.formEmp.controls.address.value,
      assignedComplains: this.action === 'Add' ? 0 : this.empService.getAssignedComplaintOfEmployee(this.formEmp.controls.empId.value),
      status: true
    };
    if (this.action === 'Add') {
      checkResetdata = this.empService.addEmployee(empModel);
      this.onReset();
      this.submitted = false;
      if (!checkResetdata) {
        this.popupService.activateVisibleState();
        this.popupService.setErrorMessage('Username Already Exists.');
      }
    } else if (this.action === 'Update') {
      empModel.status = this.dataEmp.empStatus.value === 'Active';
      checkResetdata = this.empService.updateEmployee(empModel);

    }
    this.submitted = false;
    if (checkResetdata) {
      this.onReset();
    }
  }
}

