import { Component, OnInit } from '@angular/core';
import { AreaService } from '../../../common/service/area_service/area.service';
import { CategoryService } from '../../../common/service/category_service/category.service';
import { EmployeeService } from '../../../common/service/employee_service/employee.service';
import { UserService } from '../../../common/service/user_service/user.service';
import { ComplaintService } from '../../../common/service/complaint_service/complaint.service';
import { LoginStateModel } from 'src/app/common/model/loginstate_model/login-state-model';
import { Router } from '@angular/router';
import { LoginStateService } from 'src/app/common/service/login_state/loginstate.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  areas: any;
  categories: any;
  employee: any;
  users: any;
  complain: any;

  constructor(
    private areaService: AreaService,
    private categoryService: CategoryService,
    private employeeService: EmployeeService,
    private userService: UserService,
    private complainService: ComplaintService,
    public router: Router,
    private loginStateService: LoginStateService
  ) {
  }

  ngOnInit() {
    if (this.loginStateService.isLoginStateValidOfUser('Admin')) {
      this.router.navigate(['login']);
      return;
    }
    this.areas = this.areaService.getServiceableArea();
    this.categories = this.categoryService.getServiceableCategory();
    this.employee = this.employeeService.getEmployeeStatus();
    this.users = this.userService.getUserStatus();
    this.complain = this.complainService.getComplainStatus();
  }
}


