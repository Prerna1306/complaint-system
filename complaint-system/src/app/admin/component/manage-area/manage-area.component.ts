import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AreaModel } from '../../../common/model/area_model/area-model';
import { AreaService } from '../../../common/service/area_service/area.service';
import { LoginStateService } from 'src/app/common/service/login_state/loginstate.service';
import { Router } from '@angular/router';
import { PopupService } from '../../../common/service/popup_service/popup.service';

@Component({
  selector: 'app-manage-area',
  templateUrl: './manage-area.component.html',
  styleUrls: ['./manage-area.component.css']
})
export class ManageAreaComponent implements OnInit {
  areaForm: FormGroup;
  submitted = false;
  areaData: AreaModel[];
  action = 'Add';
  areaNameExists = false;
  selectedEmployeeId = 0;
  prevName = '';
  constructor(
    private formBuilder: FormBuilder,
    private areaService: AreaService,
    private loginStateService: LoginStateService,
    private router: Router,
    private popupService: PopupService
  ) {
    this.areaForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(50)]],
      pincode: ['', [Validators.required, Validators.pattern('[0-9]{6}')]],
      description: ['', [Validators.required, Validators.maxLength(255)]],
      areaId: [''], status: ['Active'],
    });
    if (this.loginStateService.isLoginStateValidOfUser('Admin')) {
      this.router.navigate(['login']);
      return;
    }
  }

  ngOnInit() {
    this.setAreaData();
  }

  onSubmit() {
    if (this.areaForm.invalid) {
      this.submitted = true;
      return;
    }
    if (this.action === 'Add') {
      {
        this.areaNameExists = this.areaService.isAreaUnique(this.formFields.name.value);
        if (this.areaNameExists) {
          return;
        }
        const area: AreaModel = {
          areaId: this.areaService.getLastAreaId(),
          name: this.formFields.name.value,
          pinCode: this.formFields.pincode.value,
          description: this.formFields.description.value,
          status: 1
        };
        this.areaService.addNewArea(area);
        this.setAreaData();
        this.areaForm.reset();
        this.submitted = false;
        this.areaNameExists = false;
        this.popupService.setSuccessMessage('Successfully Added Area !!');
        this.popupService.activateVisibleState();
      }
    } else if (this.action === 'Update') {
      if (this.prevName === this.formFields.name.value) {
        this.areaNameExists = true;
        this.updateAreaData();
      } else if (this.areaService.isAreaUnique(this.formFields.name.value)) {
        this.areaNameExists = true;
      } else {
        this.areaNameExists = true;
        this.updateAreaData();
      }
    }
  }

  setAreaData() {
    this.areaData = JSON.parse(localStorage.getItem('AREADATA'));
  }

  get formFields() {
    return this.areaForm.controls;
  }

  display(areaD: AreaModel) {
    this.areaNameExists = false;
    this.action = 'Update';
    this.areaForm.patchValue(
      {
        areaId: areaD.areaId,
        name: areaD.name,
        pincode: areaD.pinCode,
        description: areaD.description,
        status: areaD.status
      });
    this.prevName = areaD.name;
    this.selectedEmployeeId = areaD.areaId;
  }

  updateAreaData() {
    this.selectedEmployeeId = 0;
    this.areaService.updateDataToLocalStorage({
      areaId: 0, name: this.formFields.name.value,
      pinCode: this.formFields.pincode.value,
      description: this.formFields.description.value,
      status: this.formFields.status.value === 'Active' ? 1 : 0
    }, this.formFields.areaId.value);
    this.setAreaData();
    this.submitted = false;
    this.areaForm.reset();
    this.action = 'Add';
    this.areaNameExists = false;
    this.popupService.setSuccessMessage('Successfully Area Updated !!');
    this.popupService.activateVisibleState();
  }
}
