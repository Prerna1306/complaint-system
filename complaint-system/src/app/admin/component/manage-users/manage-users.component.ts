import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserModel } from '../../../common/model/user_model/user-model';
import { UserService } from 'src/app/common/service/user_service/user.service';
import { LoginStateService } from 'src/app/common/service/login_state/loginstate.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.css']
})
export class ManageUsersComponent implements OnInit {
  userdetail: UserModel[];
  constructor(
    private userService: UserService,
    private httpService: HttpClient,
    private loginStateService: LoginStateService,
    private router: Router
  ) {
    if (this.loginStateService.isLoginStateValidOfUser('Admin')) {
      this.router.navigate(['login']);
      return;
    }
  }

  ngOnInit() {
  }

  getUserdetails() {
    return this.userService.getAllDataFromLocalStorage();
  }

  checkStatus(userId) {
    this.userService.updateDataToLocalStorage(userId);
  }
}
