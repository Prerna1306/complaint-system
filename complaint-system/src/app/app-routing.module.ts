import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/component/login/login.component';
import { AdminComponent } from './admin/component/admin/admin.component';
import { EmployeeComponent } from './employee/component/employee/employee.component';
import { UserComponent } from './user/component/user/user.component';
import { PageNotFoundComponent } from './common/component/page-not-found/page-not-found.component';
import { SignupComponent } from './user/component/signup/signup.component';
import { RegisterComplaintComponent } from './user/component/register-complaint/register-complaint.component';
import { HomeComponent } from './common/component/home/home.component';
import { ManageAreaComponent } from './admin/component/manage-area/manage-area.component';
import { ManageCategoriesComponent } from './admin/component/manage-categories/manage-categories.component';
import { ChangePasswordComponent } from './common/component/change-password/change-password.component';
import { PastComplaintDetailComponent } from './user/component/past-complaint-detail/past-complaint-detail.component';
import { UserProfileComponent } from './user/component/user-profile/user-profile.component';
import { ManageEmployeeComponent } from './admin/component/manage-employee/manage-employee.component';
import { ManageUsersComponent } from './admin/component/manage-users/manage-users.component';
import { ManageComplaintStatusComponent } from './employee/component/manage-complaint-status/manage-complaint-status.component';
import { ManageComplaintComponent } from './admin/component/manage-complaint/manage-complaint.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'area', component: ManageAreaComponent },
  { path: 'category', component: ManageCategoriesComponent },
  { path: 'employee', component: EmployeeComponent },
  { path: 'user', component: UserComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'newcomplaint', component: RegisterComplaintComponent },
  { path: 'changepassword', component: ChangePasswordComponent },
  { path: 'pastcomplaint', component: PastComplaintDetailComponent },
  { path: 'profile', component: UserProfileComponent },
  { path: 'manageemployee', component: ManageEmployeeComponent },
  { path: 'managecomplaintstatus/:id', component: ManageComplaintStatusComponent },
  { path: 'manageusers', component: ManageUsersComponent },
  { path: 'acomplaint', component: ManageComplaintComponent },
  { path: '**', component: PageNotFoundComponent }

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
