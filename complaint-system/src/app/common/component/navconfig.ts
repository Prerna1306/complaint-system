export const menus = [
  {
    url: '/home',
    menulist: [
      {
        label: 'Home',
        routerLink: '/home'
      },
      {
        label: 'New Complaint',
        routerLink: '/newcomplaint'
      },
      {
        label: 'Sign Up',
        routerLink: '/signup'
      },
      {
        label: 'Login',
        routerLink: '/login'
      }
    ]
  },
  {
    url: '/user',
    menulist: [
      {
        label: 'Home',
        routerLink: '/user'
      },
      {
        label: 'New Complaint',
        routerLink: '/newcomplaint'
      },
      {
        label: 'My Complaint',
        routerLink: '/pastcomplaint'
      },
      {
        label: 'My Profile',
        routerLink: '/profile'
      },
      {
        label: 'Change Password',
        routerLink: '/changepassword'
      },
      {
        label: 'Logout',
        routerLink: '/'
      }
    ]
  },
  {
    url: '/admin',
    menulist: [
      {
        label: 'Home',
        routerLink: '/admin'
      },
      {
        label: 'Manage Complaints',
        routerLink: '/acomplaint'
      },
      {
        label: 'Manage Categories',
        routerLink: '/category'
      },
      {
        label: 'Manage Area',
        routerLink: '/area'
      },
      {
        label: 'Manage User',
        routerLink: '/manageusers'
      },
      {
        label: 'Manage Employee',
        routerLink: '/manageemployee'
      },
      {
        label: 'Logout',
        routerLink: '/'
      }
    ]
  },
  {
    url: '/employee',
    menulist: [
      {
        label: 'Home',
        routerLink: '/employee'
      },
      {
        label: 'Change Password',
        routerLink: '/changepassword'
      },
      {
        label: 'Logout',
        routerLink: '/'
      }
    ]
  },
  {
    url: '/changepassword',
    menulist: [
      {
        label: 'Home',
        routerLink: '/login'
      }
    ]
  }
];
