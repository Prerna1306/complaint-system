import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { menus } from 'src/app/common/component/navconfig';
import { LoginStateService } from '../../service/login_state/loginstate.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  menu = menus;
  greeting: string;

  constructor(public route: Router, protected loginStateService: LoginStateService) {
    // tslint:disable-next-line: one-variable-per-declaration
    const data = [
      [0, 12, 'Good Morning'],
      [12, 18, 'Good Afternoon'],
      [18, 24, 'Good Evening']
    ],
      hr = new Date().getHours();
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < data.length; i++) {
      if (hr >= data[i][0] && hr <= data[i][1]) {
        const arr = data[i][2];
        this.greeting = arr.toString();
        break;
      }
    }
  }

  ngOnInit() { }

  getMenutoDisplay(): any[] {
    if (this.checkLoginState('User')) {
      return this.menu.filter(it => it.url.includes('/user'));
    } else if (this.checkLoginState('Employee')) {
      return this.menu.filter(it => it.url.includes('/employee'));
    } else if (this.checkLoginState('Admin')) {
      return this.menu.filter(it => it.url.includes('/admin'));
    } else {
      return this.menu.filter(it => it.url.includes('/home'));
    }
  }

  logout() {
    this.loginStateService.destroySession();
    this.route.navigate(['/login']);
  }

  checkLoginState(role: string): boolean {
    return (this.loginStateService.loginState && this.loginStateService.loginState.role === role);
  }
}
