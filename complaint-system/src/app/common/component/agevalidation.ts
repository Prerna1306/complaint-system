import { FormGroup, AbstractControl } from '@angular/forms';

export function ValidAge(controlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        if (control.errors && control.errors.validAge) {
            return;
        }

        // set error
        const currentDate = new Date();
        if (control.value) {
            const dob = new Date(control.value);
            const dobYear = dob.getFullYear();
            const maxDobYear = currentDate.getFullYear() - 18;
            if (maxDobYear < dobYear) {
                control.setErrors({ validAge: true });
            } else {
                control.setErrors(null);
            }
        }
    };
}
