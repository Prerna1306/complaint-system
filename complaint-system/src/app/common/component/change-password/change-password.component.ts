import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../service/user_service/user.service';
import { LoginStateService } from '../../service/login_state/loginstate.service';
import { Router } from '@angular/router';
import { MustMatch } from '../change-password/confirm-password-validation';
import { EmployeeService } from '../../service/employee_service/employee.service';
import { PopupService } from '../../service/popup_service/popup.service';
@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  changePasswordForm: FormGroup;
  submitted = false;
  flag = true;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private employeeService: EmployeeService,
    private loginStateService: LoginStateService,
    private router: Router,
    private popupService: PopupService
  ) {
    if (this.loginStateService.loginState === undefined
      || (this.loginStateService.loginState !== undefined && this.loginStateService.loginState.role === 'Admin')) {
      this.router.navigate(['login']);
      return;
    }
  }

  ngOnInit() {
    this.changePasswordForm = this.formBuilder.group({
      currentpassword: ['', [Validators.required, Validators.minLength(4)]],
      newpassword: ['', [Validators.required, Validators.minLength(4)]],
      confirmpassword: ['', [Validators.required, Validators.minLength(4)]]
    }, {
      validator: MustMatch('newpassword', 'confirmpassword')
    });
  }

  get formcontrol() {
    return this.changePasswordForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.changePasswordForm.invalid) {
      return;
    }
    if (this.loginStateService.loginState.role === 'User') {
      if (this.updateUserPassword()) {
        return;
      }
    } else if (this.loginStateService.loginState.role === 'Employee') {
      if (this.updateEmployeePassword()) {
        return;
      }
    }
    this.popupService.setSuccessMessage(`Your password updated succesfully. Please login again.`);
    this.popupService.activateVisibleState();
    this.loginStateService.destroySession();
    this.router.navigate(['/login']);
  }

  updateUserPassword() {
    this.changePasswordForm.valueChanges.subscribe(data => this.flag = true);
    return this.userService.verifyPassword(this.loginStateService.loginState.id, this.formcontrol.currentpassword.value)
      ? this.userService.updatePassword(this.loginStateService.loginState.id, this.formcontrol.confirmpassword.value)
      : this.flag = !this.flag;
  }

  updateEmployeePassword() {
    this.changePasswordForm.valueChanges.subscribe(data => this.flag = true);
    return this.employeeService.verifyPassword(this.loginStateService.loginState.id, this.formcontrol.currentpassword.value)
      ? this.employeeService.updatePassword(this.loginStateService.loginState.id, this.formcontrol.confirmpassword.value)
      : this.flag = !this.flag;
  }

  onReset() {
    this.submitted = false;
    this.changePasswordForm.reset();
  }

}
