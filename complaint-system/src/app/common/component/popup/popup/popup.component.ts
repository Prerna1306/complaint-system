import { Component, OnInit } from '@angular/core';
import { PopupService } from '../../../service/popup_service/popup.service';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {
  constructor(private popupService: PopupService) { }

  ngOnInit() {
  }

  hidePopDiv() {
    this.popupService.deactivateVisibleState();
  }
}
