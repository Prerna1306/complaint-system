export class ComplainModel {
    compId: number;
    catId: number;
    userId: number;
    empId: number;
    subject: string;
    shortDescription: string;
    areaId: number;
    description: string;
    compDate: string;
    completeDate: string;
    status: string;
}
