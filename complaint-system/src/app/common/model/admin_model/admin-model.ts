export class AdminModel {
    adminid: number;
    userName: string;
    password: string;
    email: string;
    contact: number;
}
