export class EmployeeModel {
    empId: number;
    firstName: string;
    lastName: string;
    userName: string;
    password: string;
    email: string;
    assignedComplains: number;
    address: string;
    status: boolean;
}


