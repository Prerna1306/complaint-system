export class SolutionModel {
    solutionId: number;
    empId: number;
    complainId: number;
    solution: string;
}
