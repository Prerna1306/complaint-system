export class LoginStateModel {
    role: string;
    id: number;
    username: string;
}