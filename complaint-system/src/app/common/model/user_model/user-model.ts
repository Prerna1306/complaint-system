export class UserModel {
  userId: number;
  userName: string;
  password: string;
  fName: string;
  lName: string;
  email: string;
  contact: number;
  dob: string;
  address: string;
  city: string;
  state: string;
  pincode: number;
  status: boolean;
}
