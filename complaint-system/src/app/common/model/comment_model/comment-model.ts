export class CommentModel {
    commentId: number;
    userId: number;
    complainId: number;
    comment: string;
}
