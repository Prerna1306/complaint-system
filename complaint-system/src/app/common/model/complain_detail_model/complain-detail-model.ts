export class ComplainDetailModel {
    compId: number;
    category: { catId: number, catName: string };
    userId: number;
    employee: { empId: number, empName: string };
    subject: string;
    shortdescription: string;
    area: { areaId: number, areaName: string };
    description: string;
    compDate: string;
    completeDate: string;
    comments: { username: string, comment: string }[];
    solution: string;
    status: string;
}

