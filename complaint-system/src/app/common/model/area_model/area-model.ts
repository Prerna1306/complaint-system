export class AreaModel {
    areaId: number;
    name: string;
    pinCode: number;
    description: string;
    status: number;
}

