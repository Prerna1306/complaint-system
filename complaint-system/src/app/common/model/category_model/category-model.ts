export class CategoryModel {
    catId: number;
    catName: string;
    description: string;
    status: number;
}
