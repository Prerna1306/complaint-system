import { Injectable, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CommentModel } from '../../model/comment_model/comment-model';
import { SolutionModel } from '../../model/solution_model/solution-model';

@Injectable({
  providedIn: 'root'
})
export class SolutionService implements OnDestroy {

  path = '/assets/dummy_data/Solution.json';
  key = 'SOLUTIONDATA';
  dataSubscription: Subscription;

  constructor(private http: HttpClient) {
    if (!localStorage.getItem(this.key)) {
      this.setAllDataToLocalStorage();
    }
  }

  setAllDataToLocalStorage() {
    this.dataSubscription = this.http.get<SolutionModel[]>(this.path)
      .subscribe(data => localStorage.setItem(this.key, JSON.stringify(data)));
    return this.dataSubscription;
  }

  getAllDataFromLocalStorage(): SolutionModel[] {
    return JSON.parse(localStorage.getItem(this.key));
  }

  getSolutionByComplainId(complainId: number): string {
    const solution: SolutionModel = this.getAllDataFromLocalStorage()
      .find(data => data.complainId === complainId);
    if (solution === undefined) {
      return '';
    } else {
      return solution.solution;
    }
  }

  addNewSolution(solutionObj: SolutionModel) {
    const solutions: SolutionModel[] = this.getAllDataFromLocalStorage();
    solutions.push(solutionObj);
    localStorage.setItem(this.key, JSON.stringify(solutions));
  }

  getLastSolutionId(): number {
    const solution: SolutionModel[] = this.getAllDataFromLocalStorage();
    return solution[solution.length - 1].solutionId + 1;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }
}
