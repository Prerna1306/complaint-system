import { Injectable, OnDestroy } from '@angular/core';
import { EmployeeModel } from '../../model/employee_model/employee-model';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { ComplainModel } from '../../model/complain_model/complain-model';
import { ComplaintService } from 'src/app/common/service/complaint_service/complaint.service';
import { PopupService } from '../popup_service/popup.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService implements OnDestroy {
  path = '/assets/dummy_data/employee_data.json';
  key = 'EMPDATA';
  dataSubscription: Subscription;


  constructor(private http: HttpClient, private complaintService: ComplaintService, private popupService: PopupService) {
    if (!localStorage.getItem(this.key)) {
      this.setAllDataToLocalStorage();
    }
  }

  setAllDataToLocalStorage() {
    this.dataSubscription = this.http.get<EmployeeModel[]>(this.path)
      .subscribe(data => localStorage.setItem(this.key, JSON.stringify(data)));
    return this.dataSubscription;
  }

  getNextId(): number {
    let id = 0;
    this.getAllDataFromLocalStorage().forEach(element => {
      if (element.empId >= id) {
        id = element.empId + 1;
      }
    });
    return id;
  }

  getAssignedComplaintOfEmployee(empId: number): number {
    return this.getAllDataFromLocalStorage().find(data => data.empId === empId).assignedComplains;
  }

  getAllDataFromLocalStorage(): EmployeeModel[] {
    return JSON.parse(localStorage.getItem(this.key));
  }

  getEmployeeStatus(): any {
    const employeeStatistics = {
      activeEmployeeCount: 0,
      inActiveEmployeeCount: 0
    };
    const employee: EmployeeModel[] = this.getAllDataFromLocalStorage();
    const activeEmployee = employee.filter(data => data.status === true);
    employeeStatistics.activeEmployeeCount = activeEmployee.length;
    employeeStatistics.inActiveEmployeeCount = employee.length - activeEmployee.length;
    return employeeStatistics;
  }

  getEmployeeNameById(id: number): string {
    const employees: EmployeeModel[] = this.getAllDataFromLocalStorage();
    const employee = employees.find(data => data.empId === id);
    if (employee !== undefined) {
      return employee.userName;
    }
    return null;
  }

  verifyPassword(id: number, password: string): boolean {
    const employees: EmployeeModel[] = this.getAllDataFromLocalStorage();
    const i = employees.findIndex(data => data.empId === id);
    return employees[i].password === password ? true : false;
  }

  updatePassword(id, newPassword) {
    const employees: EmployeeModel[] = this.getAllDataFromLocalStorage();
    const i = employees.findIndex(data => data.empId === id);
    employees[i].password = newPassword;
    localStorage.setItem(this.key, JSON.stringify(employees));
  }

  getInProgressComplainsForEachEmployee(): { empid: number, inProgressCount: number }[] {
    const employeeStatus: { empid: number, inProgressCount: number }[] = [];
    const employeeIds = this.getAllActiveEmployeeIds();
    const complains: ComplainModel[] = this.complaintService.getComplaintsDetail();
    employeeIds.forEach((data, i) => {
      employeeStatus[i] = { empid: data, inProgressCount: 0 };
      const len = complains.filter(data1 => data1.empId === data && data1.status === 'In progress').length;
      employeeStatus[i].inProgressCount = len > 0 ? len : 0;
    });
    return employeeStatus;
  }

  assignComplainToEmployee(): number {
    const employees: { empid: number, inProgressCount: number }[] = this.getInProgressComplainsForEachEmployee();
    employees.sort((a, b) => {
      return a.inProgressCount - b.inProgressCount;
    });
    return employees[0].empid;
  }

  getAllActiveEmployeeIds(): number[] {
    const employees: EmployeeModel[] = this.getAllDataFromLocalStorage();
    return employees.filter(data => data.status).map(data => data.empId);
  }

  getAllEmployeeNameAndId(): string[] {
    const employeeDetail = this.getAllDataFromLocalStorage();
    return employeeDetail.map(data => data.empId + '-' + data.userName);
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  addEmployee(employee: EmployeeModel): boolean {
    const employees: EmployeeModel[] = this.getAllDataFromLocalStorage();
    const employeeExistCheck = employees.filter(e => e.userName.toUpperCase() === employee.userName.toUpperCase()
      || e.email === employee.email);
    if (employeeExistCheck.length > 0) {
      return false;

    } else {
      const data = this.getAllDataFromLocalStorage();
      data.push(employee);
      localStorage.setItem(this.key, JSON.stringify(data));
      this.popupService.activateVisibleState();
      this.popupService.setSuccessMessage('Employee Added Succesfully.');
      return true;
    }


  }
  updateEmployee(emp: EmployeeModel): boolean {
    const employees: EmployeeModel[] = this.getAllDataFromLocalStorage();
    const employeeUpdated = employees.find(e => e.empId === emp.empId);
    const indexOfemployee = employees.indexOf(employeeUpdated);
    if (indexOfemployee >= 0) {
      const ObjectEmployee = Object.assign(employeeUpdated, emp);
      employees[indexOfemployee] = emp;
      employees.splice(indexOfemployee, 1, ObjectEmployee);
      localStorage.setItem(this.key, JSON.stringify(employees));
      this.popupService.activateVisibleState();
      this.popupService.setSuccessMessage('Employee Updated Succesfully.');
      return true;
    }
    else {
      return false;
    }
  }
}
