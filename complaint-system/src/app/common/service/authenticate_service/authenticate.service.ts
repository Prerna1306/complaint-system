import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../user_service/user.service';
import { EmployeeService } from '../employee_service/employee.service';
import { UserModel } from '../../model/user_model/user-model';
import { EmployeeModel } from '../../model/employee_model/employee-model';
import { AdminModel } from '../../model/admin_model/admin-model';
import { AdminService } from '../admin_service/admin.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticateService {

  constructor(
    private http: HttpClient,
    private adminService: AdminService,
    private employeeService: EmployeeService,
    private userService: UserService,
  ) {
  }

  authenticate(role: string, username: string, password: string): any | null {
    switch (role) {
      case 'User':
        return this.authenticateUser(username, password);

      case 'Admin':
        return this.authenticateAdmin(username, password);

      case 'Employee':
        return this.authenticateEmployee(username, password);
    }
  }

  authenticateAdmin(username: string, password: string): any | null {
    const admins: AdminModel[] = this.adminService.getAllDataFromLocalStorage();
    const admin = admins.find(data => data.userName === username && data.password === password);
    return admin != null
      ? { role: 'Admin', adminid: admin.adminid, username: admin.userName }
      : null;
  }

  authenticateEmployee(username: string, password: string): any | null {
    const employees: EmployeeModel[] = this.employeeService.getAllDataFromLocalStorage();
    const employee = employees.find(data => data.userName === username && data.password === password);
    return employee != null
      ? { role: 'Employee', empid: employee.empId, username: employee.userName, status: employee.status }
      : null;
  }

  authenticateUser(username: string, password: string): any | null {
    const users: UserModel[] = this.userService.getAllDataFromLocalStorage();
    const user = users.find(data => data.userName === username && data.password === password);
    return user != null
      ? { role: 'User', userid: user.userId, username: user.userName, status: user.status }
      : null;
  }
}
