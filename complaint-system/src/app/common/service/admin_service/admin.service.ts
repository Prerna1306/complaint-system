import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AdminModel } from '../../model/admin_model/admin-model';
import { Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService implements OnDestroy {

  path = '/assets/dummy_data/admin_data.json';
  key = 'ADMINDATA';
  dataSubscription: Subscription;

  constructor(private http: HttpClient) {
    if (!localStorage.getItem(this.key)) {
      this.setAllDataToLocalStorage();
    }
  }

  setAllDataToLocalStorage() {
    this.dataSubscription = this.http.get<AdminModel[]>(this.path)
      .subscribe(data => localStorage.setItem(this.key, JSON.stringify(data)));
    return this.dataSubscription;
  }

  getAllDataFromLocalStorage(): AdminModel[] {
    return JSON.parse(localStorage.getItem(this.key));
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }
}
