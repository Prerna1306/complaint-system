import { Injectable } from '@angular/core';
import { AreaService } from '../area_service/area.service';
import { EmployeeService } from '../employee_service/employee.service';
import { CategoryService } from '../category_service/category.service';
import { ComplainDetailModel } from '../../model/complain_detail_model/complain-detail-model';
import { ComplaintService } from '../complaint_service/complaint.service';
import { ComplainModel } from '../../model/complain_model/complain-model';
import { CommentService } from '../comments_service/comment.service';
import { SolutionService } from '../solution_servie/solution.service';

@Injectable({
  providedIn: 'root'
})
export class ComplainDetailService {

  constructor(
    private areaService: AreaService,
    private employeeService: EmployeeService,
    private categoryService: CategoryService,
    private complaintService: ComplaintService,
    private commentService: CommentService,
    private solutionService: SolutionService,
  ) { }

  getComplainDetailsByAreaId(areaId: number): ComplainDetailModel[] {
    const complainModel: ComplainModel[] = this.complaintService.getComplaintsByAreaId(areaId);
    const complainDetailModel: ComplainDetailModel[] = [];
    complainModel.forEach((data, i) => {
      if (data.areaId === areaId) {
        this.fillComplainDetailModel(i, data, complainDetailModel);
      }
    });
    return complainDetailModel;
  }

  getComplainDetailsByCategoryId(categoryId: number): ComplainDetailModel[] {
    const complainModel: ComplainModel[] = this.complaintService.getComplaintsByCategoryId(categoryId);
    const complainDetailModel: ComplainDetailModel[] = [];
    complainModel.forEach((data, i) => {
      if (data.catId === categoryId) {
        this.fillComplainDetailModel(i, data, complainDetailModel);
      }
    });
    return complainDetailModel;
  }

  getComplainDetailsByUserId(userId: number): ComplainDetailModel[] {
    const complainModel: ComplainModel[] = this.complaintService.getComplaintsByUserId(userId);
    const complainDetailModel: ComplainDetailModel[] = [];
    complainModel.forEach((data, i) => {
      if (data.userId === userId) {
        this.fillComplainDetailModel(i, data, complainDetailModel);
      }
    });
    return complainDetailModel;
  }

  getComplainDetailsByEmployeeId(empId: number): ComplainDetailModel[] {
    const complainModel: ComplainModel[] = this.complaintService.getComplaintsByEmployeeId(empId);
    const complainDetailModel: ComplainDetailModel[] = [];
    complainModel.forEach((data, i) => {
      if (data.empId === empId) {
        this.fillComplainDetailModel(i, data, complainDetailModel);
      }
    });
    return complainDetailModel;
  }

  getComplainDetailsByStatus(status: string): ComplainDetailModel[] {
    const complainModel: ComplainModel[] = this.complaintService.getComplaintByStatus(status);
    const complainDetailModel: ComplainDetailModel[] = [];
    complainModel.forEach((data, i) => {
      this.fillComplainDetailModel(i, data, complainDetailModel);
    });
    return complainDetailModel;
  }

  getLatestComplainDetails(totalListItems: number): ComplainDetailModel[] {
    const complainModel: ComplainModel[] = this.complaintService.getLatestComplain(totalListItems);
    const complainDetailModel: ComplainDetailModel[] = [];
    complainModel.forEach((data, i) => {
      this.fillComplainDetailModel(i, data, complainDetailModel);
    });
    return complainDetailModel;
  }

  getComplainDetailsByComplainId(compId: number): ComplainDetailModel {
    const complainModel: ComplainModel = this.complaintService.getComplaintByComplainId(compId);
    const complainDetailModel: ComplainDetailModel = new ComplainDetailModel();
    complainDetailModel.compId = complainModel.compId;
    complainDetailModel.category = { catId: complainModel.catId, catName: this.categoryService.getCategoryNameById(complainModel.catId) };
    complainDetailModel.userId = complainModel.userId;
    complainDetailModel.employee = { empId: complainModel.empId, empName: this.employeeService.getEmployeeNameById(complainModel.empId) };
    complainDetailModel.subject = complainModel.subject;
    complainDetailModel.shortdescription = complainModel.shortDescription;
    complainDetailModel.area = { areaId: complainModel.areaId, areaName: this.areaService.getAreaNameById(complainModel.areaId) };
    complainDetailModel.description = complainModel.description;
    complainDetailModel.compDate = complainModel.compDate;
    complainDetailModel.completeDate = complainModel.completeDate;
    complainDetailModel.comments = null;
    complainDetailModel.solution = this.solutionService.getSolutionByComplainId(complainModel.compId);
    complainDetailModel.status = complainModel.status;
    return complainDetailModel;
  }

  fillComplainDetailModel(index, data, array: ComplainDetailModel[]) {
    array[index] = this.getComplainDetailsByComplainId(data.compId);
  }
}
