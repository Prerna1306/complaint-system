import { TestBed } from '@angular/core/testing';

import { ComplainDetailService } from './complain-detail.service';

describe('ComplainDetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ComplainDetailService = TestBed.get(ComplainDetailService);
    expect(service).toBeTruthy();
  });
});
