import { Injectable, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AreaModel } from '../../model/area_model/area-model';

@Injectable({
  providedIn: 'root'
})
export class AreaService implements OnDestroy {

  path = '/assets/dummy_data/Area.json';
  key = 'AREADATA';
  dataSubscription: Subscription;
  STATUS_ACTIVE = 1;
  constructor(private http: HttpClient) {
    if (!localStorage.getItem(this.key)) {
      this.setAllDataToLocalStorage();
    }
  }

  setAllDataToLocalStorage() {
    this.dataSubscription = this.http.get<AreaModel[]>(this.path)
      .subscribe(data => localStorage.setItem(this.key, JSON.stringify(data)));
    return this.dataSubscription;
  }

  updateAllDataToLocalStorage(myData) {
    localStorage.setItem(this.key, myData);
  }

  getAllDataFromLocalStorage(): AreaModel[] {
    return JSON.parse(localStorage.getItem(this.key));
  }

  getServiceableArea(): any {
    const areaStatistics = {
      activeAreaCount: 0,
      inActiveAreaCount: 0
    };
    const areas: AreaModel[] = this.getAllDataFromLocalStorage();
    const activeArea = areas.filter(data => data.status === 1);
    areaStatistics.activeAreaCount = activeArea.length;
    areaStatistics.inActiveAreaCount = areas.length - activeArea.length;
    return areaStatistics;
  }

  addNewArea(data) {
    const area: AreaModel[] = this.getAllDataFromLocalStorage();
    area.push(data);
    localStorage.setItem(this.key, JSON.stringify(area));
  }

  getLastAreaId(): number {
    const area: AreaModel[] = this.getAllDataFromLocalStorage();
    return area[area.length - 1].areaId + 1;
  }

  updateDataToLocalStorage(updatedArea: AreaModel, areaId: number) {
    const areas: AreaModel[] = this.getAllDataFromLocalStorage();
    const areaToBeUpdated = areas.find(e => e.areaId === areaId);
    areaToBeUpdated.name = updatedArea.name;
    areaToBeUpdated.pinCode = updatedArea.pinCode;
    areaToBeUpdated.description = updatedArea.description;
    areaToBeUpdated.status = updatedArea.status;
    localStorage.setItem(this.key, JSON.stringify(areas));
  }

  getAreaNameById(id: number): string {
    const areas: AreaModel[] = this.getAllDataFromLocalStorage();
    const area = areas.find(data => data.areaId === id);
    if (area !== undefined) {
      return area.name;
    }
    return null;
  }

  getAreaNameByPincode(pincode: number) {
    const areas: AreaModel[] = this.getAllDataFromLocalStorage();
    const area = areas.find(data => data.pinCode === pincode);
    if (area !== undefined) {
      return area.name;
    }
    return null;
  }

  getAreaNames(): string[] {
    const areas: AreaModel[] = this.getAllDataFromLocalStorage();
    return areas.map(data => data.name);
  }
  getAllActiveAreas(): AreaModel[] {
    return this.getAllDataFromLocalStorage().filter(data => data.status === this.STATUS_ACTIVE);
  }

  getAreaIdByName(areaName: string): number {
    const areas: AreaModel[] = this.getAllDataFromLocalStorage();
    const area = areas.find(data => data.name === areaName);
    return area.areaId;
  }

  isAreaUnique(areaName: string): boolean {
    return this.getAllDataFromLocalStorage()
      .find(data => data.name.toLowerCase() === areaName.toLowerCase())
      ? true
      : false;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }
}
