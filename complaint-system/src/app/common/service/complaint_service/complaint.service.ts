import { Injectable, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ComplainModel } from '../../model/complain_model/complain-model';
import { EmployeeService } from 'src/app/common/service/employee_service/employee.service';
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ComplaintService implements OnDestroy {

  url = '../../../assets/dummy_data/Complaint.json';
  key = 'COMPLAINDATA';
  dataSubscription: Subscription;

  constructor(
    private http: HttpClient,
    private datePipe: DatePipe,
  ) {
    if (!localStorage.getItem(this.key)) {
      this.setAllDataToLocalStorage();
    }
  }

  setAllDataToLocalStorage() {
    this.dataSubscription = this.http.get<ComplainModel[]>(this.url)
      .subscribe(data => localStorage.setItem(this.key, JSON.stringify(data)));
    return this.dataSubscription;
  }

  registerNewComplain(myData) {
    const complaints: ComplainModel[] = this.getComplaintsDetail();
    complaints.push(myData);
    localStorage.setItem(this.key, JSON.stringify(complaints));
  }

  getLastCompId(): number {
    const complain: ComplainModel[] = this.getComplaintsDetail();
    return complain[complain.length - 1].compId;
  }

  getComplaintsDetail(): ComplainModel[] {
    return JSON.parse(localStorage.getItem(this.key));
  }

  getComplainStatus(): any {
    const complaintStatistics = {
      inProgressComplaintsCount: 0,
      solvedComplaintsCount: 0,
      fakeComplaintCount: 0
    };
    const complain: ComplainModel[] = this.getComplaintsDetail();
    let fakeComplain = 0;
    let activeComplain = 0;
    let solvedComplain = 0;
    complain.forEach(data => {
      if (data.status === 'In progress') {
        activeComplain++;
      } else if (data.status === 'Fake') {
        fakeComplain++;
      } else {
        solvedComplain++;
      }
    });
    complaintStatistics.inProgressComplaintsCount = activeComplain;
    complaintStatistics.solvedComplaintsCount = solvedComplain;
    complaintStatistics.fakeComplaintCount = fakeComplain;
    return complaintStatistics;
  }

  getStatusOfCompliainByComplainId(complainid: number): string {
    const complain: ComplainModel = this.getComplaintsDetail()
      .find(data => data.compId === complainid);
    if (complain !== undefined) {
      return complain.status;
    }
    return null;
  }

  updateStatusOfCompliainByComplainId(complainid: number, status: string) {
    const complain: ComplainModel[] = this.getComplaintsDetail();
    const index: number = complain
      .findIndex((data, i) => (data.compId === complainid));
    complain[index].status = status;
    localStorage.setItem(this.key, JSON.stringify(complain));
  }

  updateCompletetDateOfCompliainByComplainId(complainId: number) {
    const complain: ComplainModel[] = this.getComplaintsDetail();
    const index: number = complain
      .findIndex((data, i) => (data.compId === complainId));
    let date = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    complain[index].completeDate = date;
    localStorage.setItem(this.key, JSON.stringify(complain));
  }

  getLatestComplain(totalItems: number): ComplainModel[] {
    const complains: ComplainModel[] = this.getComplaintsDetail();
    const length = complains.length < totalItems ? complains.length : totalItems;
    return complains.slice(complains.length - length);
  }

  getComplaintsByAreaId(areaid: number): ComplainModel[] {
    return this.getFilteredComplains('areaId', areaid);
  }

  getComplaintsByCategoryId(catid: number): ComplainModel[] {
    return this.getFilteredComplains('catId', catid);
  }

  getComplaintsByUserId(userid: number): ComplainModel[] {
    return this.getFilteredComplains('userId', userid);
  }

  getComplaintsByEmployeeId(empid: number): ComplainModel[] {
    return this.getFilteredComplains('empId', empid);
  }

  getComplaintByComplainId(compId): ComplainModel {
    return this.getComplaintsDetail().find(data => data.compId === compId);
  }

  getComplaintByStatus(status: string): ComplainModel[] {
    const complains: ComplainModel[] = this.getComplaintsDetail();
    return complains.filter(data => data.status === status);
  }

  getFilteredComplains(filterBy: string, id: number): ComplainModel[] {
    const complains: ComplainModel[] = this.getComplaintsDetail();
    return complains.filter(data => data[filterBy] === id);
  }

  getAllStatusTypes(): string[] {
    return ['In progress', 'Fake', 'Solved'];
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }
}

