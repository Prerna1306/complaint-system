import { TestBed } from '@angular/core/testing';

import { LoginStateService } from './loginstate.service';

describe('LoginstateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoginStateService = TestBed.get(LoginStateService);
    expect(service).toBeTruthy();
  });
});
