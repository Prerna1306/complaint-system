import { Injectable } from '@angular/core';
import { LoginStateModel } from '../../model/loginstate_model/login-state-model';
import { PopupService } from '../popup_service/popup.service';

@Injectable({
  providedIn: 'root'
})
export class LoginStateService {

  loginState: LoginStateModel;

  constructor(private popupService: PopupService) { }

  createSession({ role, id, username }) {
    const state: LoginStateModel = { role, id, username };
    sessionStorage.setItem('loginState', JSON.stringify(state));
  }

  destroySession() {
    sessionStorage.removeItem('loginState');
    this.loginState = null;
  }

  getLoginState() {
    return JSON.parse(sessionStorage.getItem('loginState'));
  }

  isLoginStateValidOfUser(currentUser: string): boolean {
    this.loginState = this.getLoginState();
    if (this.loginState === null ||
      (this.loginState !== null && this.loginState.role !== currentUser)) {
      this.popupService.setErrorMessage('You must login first !!');
      this.popupService.activateVisibleState();
      return true;
    } else {
      return false;
    }
  }
}


