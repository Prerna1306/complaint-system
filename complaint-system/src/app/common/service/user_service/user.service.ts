import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserModel } from '../../model/user_model/user-model';
import { Subscription } from 'rxjs';
import { UserModule } from 'src/app/user/user.module';

@Injectable({
  providedIn: 'root'
})
export class UserService implements OnDestroy {
  path = '/assets/dummy_data/user_data.json';
  key = 'USERDATA';
  dataSubscription: Subscription;
  constructor(private http: HttpClient) {
    if (!localStorage.getItem(this.key)) {
      this.setAllDataToLocalStorage();
    }
  }

  setAllDataToLocalStorage() {
    this.dataSubscription = this.http
      .get<UserModel[]>(this.path)
      .subscribe(data => localStorage.setItem(this.key, JSON.stringify(data)));
    return this.dataSubscription;
  }

  getAllDataFromLocalStorage(): UserModel[] {
    return JSON.parse(localStorage.getItem(this.key));
  }

  updateDataToLocalStorage(userId: number) {
    const users: UserModel[] = this.getAllDataFromLocalStorage();
    const userstatusUpdated = users.find(entity => entity.userId === userId);
    userstatusUpdated.status = !userstatusUpdated.status;
    localStorage.setItem(this.key, JSON.stringify(users));
  }

  updateAllDataFromLocalStorage(data: UserModel) {
    const user: UserModel[] = this.getAllDataFromLocalStorage();
    user.push(data);
    localStorage.setItem(this.key, JSON.stringify(user));
  }

  getLastId(): number {
    const user: UserModel[] = this.getAllDataFromLocalStorage();
    return user[user.length - 1].userId;
  }

  getUserStatus(): any {
    const userStatistics = {
      activeUserCount: 0,
      inActiveUserCount: 0
    };
    const users: UserModel[] = this.getAllDataFromLocalStorage();
    const activeUsers = users.filter(data => data.status === true);
    userStatistics.activeUserCount = activeUsers.length;
    userStatistics.inActiveUserCount = users.length - activeUsers.length;
    return userStatistics;
  }

  isUserUnique(username: string): boolean {
    return this.getAllDataFromLocalStorage()
      .find(data => data.userName === username)
      ? true
      : false;
  }

  getUsernameById(userId: number): string {
    return this.getAllDataFromLocalStorage().find(data => data.userId === userId).userName;
  }

  verifyPassword(id: number, password: string): boolean {
    const users: UserModel[] = this.getAllDataFromLocalStorage();
    const i = users.findIndex(data => data.userId === id);
    return users[i].password === password ? true : false;
  }

  updatePassword(id, newPassword) {
    const users: UserModel[] = this.getAllDataFromLocalStorage();
    const i = users.findIndex(data => data.userId === id);
    users[i].password = newPassword;
    localStorage.setItem(this.key, JSON.stringify(users));
  }

  getUserData(id): any {
    const usersdata: UserModel[] = this.getAllDataFromLocalStorage();
    const userdata = usersdata.find(res => res.userId === id);
    return userdata;
  }

  updateUserData(id, updatedata: UserModel) {
    const users: UserModel[] = this.getAllDataFromLocalStorage();
    const i = users.findIndex(data => data.userId === id);
    users[i].email = updatedata.email;
    users[i].contact = updatedata.contact;
    users[i].address = updatedata.address;
    users[i].city = updatedata.city;
    users[i].state = updatedata.state;
    users[i].pincode = updatedata.pincode;
    localStorage.setItem(this.key, JSON.stringify(users));
  }

  getAllUsersNameAndId(): string[] {
    const userDetails = this.getAllDataFromLocalStorage();
    return userDetails.map(data => data.userId + '-' + data.userName);
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

}
