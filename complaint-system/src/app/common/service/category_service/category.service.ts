import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { CategoryModel } from '../../model/category_model/category-model';

@Injectable({
  providedIn: 'root'
})
export class CategoryService implements OnDestroy {

  url = '../../../assets/dummy_data/Category.json';
  key = 'CATEGORYDATA';
  dataSubscription: Subscription;
  STATUS_ACTIVE = 1;
  constructor(private http: HttpClient) {
    if (!localStorage.getItem(this.key)) {
      this.setAllDataToLocalStorage();
    }
  }

  setAllDataToLocalStorage() {
    this.dataSubscription = this.http.get<CategoryModel[]>(this.url)
      .subscribe(data => localStorage.setItem(this.key, JSON.stringify(data)));
    return this.dataSubscription;
  }

  updateAllDataToLocalStorage(myData) {
    localStorage.setItem(this.key, myData);
  }

  getAllDataFromLocalStorage(): CategoryModel[] {
    return JSON.parse(localStorage.getItem(this.key));
  }

  getServiceableCategory(): any {
    const categoryStatistics = {
      activeCategoryCount: 0,
      inActiveCategoryCount: 0
    };
    const category: CategoryModel[] = this.getAllDataFromLocalStorage();
    const activeCategory = category.filter(data => data.status === 1);
    categoryStatistics.activeCategoryCount = activeCategory.length;
    categoryStatistics.inActiveCategoryCount = category.length - activeCategory.length;
    return categoryStatistics;
  }

  addNewCategory(data) {
    const category: CategoryModel[] = this.getAllDataFromLocalStorage();
    category.push(data);
    localStorage.setItem(this.key, JSON.stringify(category));
  }

  getLastCategoryId(): number {
    const category: CategoryModel[] = this.getAllDataFromLocalStorage();
    return category[category.length - 1].catId + 1;
  }

  updateDataToLocalStorage(updatedCategory: CategoryModel, catId: number) {
    const categories: CategoryModel[] = this.getAllDataFromLocalStorage();
    const categoryToBeUpdated = categories.find(e => e.catId === catId);
    categoryToBeUpdated.catName = updatedCategory.catName;
    categoryToBeUpdated.description = updatedCategory.description;
    categoryToBeUpdated.status = updatedCategory.status;
    localStorage.setItem(this.key, JSON.stringify(categories));
  }

  getCategoryIdByName(catName: string): number {
    const categories: CategoryModel[] = this.getAllDataFromLocalStorage();
    const category = categories.find(data => data.catName === catName);
    return category.catId;
  }

  getCategoryNameById(id: number): string {
    const categories: CategoryModel[] = this.getAllDataFromLocalStorage();
    const category = categories.find(data => data.catId === id);
    if (category !== undefined) {
      return category.catName;
    } else {
      return null;
    }
  }

  getCategoryNames(): string[] {
    const category: CategoryModel[] = this.getAllDataFromLocalStorage();
    return category.map(data => data.catName);
  }

  isCategoryUnique(categoryName: string): boolean {
    return this.getAllDataFromLocalStorage()
      .find(data => data.catName.toLowerCase() === categoryName.toLowerCase()) ? true : false;
  }

  getAllActiveCategory(): CategoryModel[] {
    return this.getAllDataFromLocalStorage().filter(data => data.status === this.STATUS_ACTIVE);
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }
}
