import { Injectable, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CommentModel } from '../../model/comment_model/comment-model';
import { UserService } from '../user_service/user.service';

@Injectable({
  providedIn: 'root'
})
export class CommentService implements OnDestroy {

  path = '/assets/dummy_data/Comment.json';
  key = 'COMMENTDATA';
  dataSubscription: Subscription;

  constructor(private http: HttpClient, private userService: UserService) {
    if (!localStorage.getItem(this.key)) {
      this.setAllDataToLocalStorage();
    }
  }

  setAllDataToLocalStorage() {
    this.dataSubscription = this.http.get<CommentModel[]>(this.path)
      .subscribe(data => localStorage.setItem(this.key, JSON.stringify(data)));
    return this.dataSubscription;
  }

  getAllDataFromLocalStorage(): CommentModel[] {
    return JSON.parse(localStorage.getItem(this.key));
  }

  getCommentsDetailByComplainId(complainId: number): { username: string, comment: string }[] {
    return this.getAllDataFromLocalStorage()
      .filter(data => data.complainId === complainId)
      .map(data => ({ username: this.userService.getUsernameById(data.userId), comment: data.comment }));
  }

  addNewComments(commentObj: CommentModel) {
    const comments: CommentModel[] = this.getAllDataFromLocalStorage();
    comments.push(commentObj);
    localStorage.setItem(this.key, JSON.stringify(comments));
  }

  getLastCommentId(): number {
    const comments: CommentModel[] = this.getAllDataFromLocalStorage();
    return comments[comments.length - 1].commentId + 1;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

}
