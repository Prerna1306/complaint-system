import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PopupService {
  visibleState: boolean;
  successMessage: string;
  errorMessage: string;

  constructor() {
    this.visibleState = false;
    this.successMessage = '';
    this.errorMessage = '';
  }

  activateVisibleState() {
    this.visibleState = true;
  }

  deactivateVisibleState() {
    this.visibleState = false;
  }

  setSuccessMessage(msg: string) {
    this.errorMessage = '';
    this.successMessage = msg;
  }

  setErrorMessage(msg: string) {
    this.successMessage = '';
    this.errorMessage = msg;
  }

  getVisibleState() {
    return this.visibleState;
  }

  getSuccessMessage() {
    return this.successMessage;
  }

  getErrorMessage() {
    return this.errorMessage;
  }
}
