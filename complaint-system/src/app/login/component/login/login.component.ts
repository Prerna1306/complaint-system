import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticateService } from '../../../common/service/authenticate_service/authenticate.service';
import { LoginStateService } from 'src/app/common/service/login_state/loginstate.service';
import { LoginStateModel } from 'src/app/common/model/loginstate_model/login-state-model';
import { PopupService } from 'src/app/common/service/popup_service/popup.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  flag = true;
  status = true;
  reset = true;
  formReset = false;
  roles = ['Admin', 'Employee', 'User'];
  // selected1 = this.roles[1];

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private authenticateService: AuthenticateService,
    private loginStateService: LoginStateService,
  ) {
    this.loginForm = this.formBuilder.group({
      role: [this.roles[2]],
      username: ['', [Validators.required, Validators.minLength(4)]],
      password: ['', [Validators.required, Validators.minLength(4)]]
    });

  }

  ngOnInit() {
    const loginState: LoginStateModel = this.loginStateService.getLoginState();
    if (loginState != null) {
      this.routeByRole(loginState.role);
    }
  }

  get formFields() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }

    // reseting the invalid credential status by subscribing to the formevents
    this.loginForm
      .valueChanges
      .subscribe((d) => {
        this.flag = null;
        this.status = null;
        this.reset = true;
        this.formReset = true;
      });

    // routing according to the role
    const user = this.authenticateService.authenticate(this.formFields.role.value,
      this.formFields.username.value, this.formFields.password.value);
    if (user != null) {
      if (user.role === 'Admin' || user.status) {
        const id = user.role === 'User'
          ? user.userid
          : user.role === 'Admin'
            ? user.adminid
            : user.empid;
        this.loginStateService.createSession({ role: this.formFields.role.value, id, username: user.username });
        this.routeByRole(this.formFields.role.value);
      } else {
        this.status = false;
      }
    } else {
      this.flag = false;
    }
  }

  onReset() {
    this.formReset = true;
    this.submitted = false;
    this.flag = true;
    this.status = true;
    this.loginForm.setValue({
      role: this.roles[2],
      username: null,
      password: null
    });
  }

  routeByRole(role: string) {
    if (role === 'User') {
      this.router.navigate(['user']);
    } else if (role === 'Admin') {
      this.router.navigate(['admin']);
    } else if (role === 'Employee') {
      this.router.navigate(['employee']);
    }
  }
}
