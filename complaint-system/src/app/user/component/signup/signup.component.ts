import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserModel } from '../../../common/model/user_model/user-model';
import { UserService } from 'src/app/common/service/user_service/user.service';
import { Router } from '@angular/router';
import { MustMatch } from 'src/app/common/component/change-password/confirm-password-validation';
import { PopupService } from 'src/app/common/service/popup_service/popup.service';
import { ValidAge } from 'src/app/common/component/agevalidation';
import { LoginStateService } from '../../../common/service/login_state/loginstate.service';
import { LoginStateModel } from '../../../common/model/loginstate_model/login-state-model';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],

})
export class SignupComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  flag = false;
  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router,
    private popupService: PopupService,
    private loginStateService: LoginStateService,
  ) {

    if (loginStateService.loginState !== null) {
      router.navigate(['login']);
      return;
    }
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmpassword: ['', [Validators.required, Validators.minLength(6)]],
      email: ['', [Validators.required, Validators.email]],
      contact: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      dob: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      pincode: ['', [Validators.required, Validators.minLength(6)]]
    }, {
      validators: [ValidAge('dob'), MustMatch('password', 'confirmpassword')]
    });
    this.registerForm.valueChanges.subscribe(data => this.flag = false);
  }

  get formcontrol() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.userService.isUserUnique(this.formcontrol.username.value)) {
      this.flag = true;
      return;
    }
    this.flag = false;
    if (this.registerForm.invalid) {
      return;
    }

    const user: UserModel = {
      userId: this.userService.getLastId() + 1,
      userName: this.registerForm.controls.username.value,
      password: this.registerForm.controls.password.value,
      fName: this.registerForm.controls.firstName.value,
      lName: this.registerForm.controls.lastName.value,
      email: this.registerForm.controls.username.value,
      contact: this.registerForm.controls.contact.value,
      dob: this.registerForm.controls.dob.value,
      address: this.registerForm.controls.address.value,
      city: this.registerForm.controls.city.value,
      state: this.registerForm.controls.state.value,
      pincode: this.registerForm.controls.pincode.value,
      status: true,
    };

    this.userService.updateAllDataFromLocalStorage(user);
    this.popupService.setSuccessMessage('Successfully Registration done !!');
    this.popupService.activateVisibleState();
    this.router.navigate(['/login']);
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
}
