import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginStateService } from '../../../common/service/login_state/loginstate.service';
import { LoginStateModel } from 'src/app/common/model/loginstate_model/login-state-model';
import { UserService } from 'src/app/common/service/user_service/user.service';
import { AppComponent } from '../../../app.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {

  greeting: string;

  constructor(
    public router: Router,
    private loginStateService: LoginStateService,
  ) {
    if (this.loginStateService.isLoginStateValidOfUser('User')) {
      this.router.navigate(['login']);
      return;
    }
  }
}
