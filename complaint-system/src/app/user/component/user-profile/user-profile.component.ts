import { Component, OnInit } from '@angular/core';
import { LoginStateModel } from 'src/app/common/model/loginstate_model/login-state-model';
import { LoginStateService } from 'src/app/common/service/login_state/loginstate.service';
import { Router } from '@angular/router';
import { UserModel } from '../../../common/model/user_model/user-model';
import { UserService } from 'src/app/common/service/user_service/user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserModule } from '../../user.module';
import { ThrowStmt } from '@angular/compiler';
import { PopupService } from 'src/app/common/service/popup_service/popup.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  profileForm: FormGroup;
  submitted = false;
  userData: UserModel;

  constructor(
    private loginStateService: LoginStateService,
    private router: Router,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private popupService: PopupService
  ) {
    if (this.loginStateService.isLoginStateValidOfUser('User')) {
      this.router.navigate(['login']);
      return;
    }
    this.userData = this.userService.getUserData(this.loginStateService.loginState.id);
  }

  ngOnInit() {
    this.profileForm = this.formBuilder.group({
      firstName: [this.userData.fName, Validators.required],
      lastName: [this.userData.lName, Validators.required],
      username: [this.userData.userName, Validators.required],
      email: [this.userData.email, [Validators.required, Validators.email]],
      contact: [this.userData.contact, [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      address: [this.userData.address, Validators.required],
      city: [this.userData.city, Validators.required],
      state: [this.userData.state, Validators.required],
      pincode: [this.userData.pincode, [Validators.required, Validators.minLength(6)]]
    });
  }

  get formcontrol() {
    return this.profileForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.profileForm.invalid) {
      return;
    }

    const user: UserModel = {
      userId: this.userData.userId,
      userName: this.userData.userName,
      password: this.userData.password,
      fName: this.userData.fName,
      lName: this.userData.lName,
      email: this.profileForm.controls.email.value,
      contact: this.profileForm.controls.contact.value,
      dob: this.userData.dob,
      address: this.profileForm.controls.address.value,
      city: this.profileForm.controls.city.value,
      state: this.profileForm.controls.state.value,
      pincode: this.profileForm.controls.pincode.value,
      status: this.userData.status,
    };
    this.userService.updateUserData(this.loginStateService.loginState.id, user);
    this.popupService.setSuccessMessage('Your profile updated succesfully.');
    this.popupService.activateVisibleState();
    this.router.navigate(['/user']);

  }




}
