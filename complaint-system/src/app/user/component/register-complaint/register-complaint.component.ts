import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from '../../../common/service/category_service/category.service';
import { AreaService } from '../../../common/service/area_service/area.service';
import { ComplaintService } from '../../../common/service/complaint_service/complaint.service';
import { DatePipe } from '@angular/common';
import { ComplainModel } from '../../../common/model/complain_model/complain-model';
import { LoginStateService } from '../../../common/service/login_state/loginstate.service';
import { Router } from '@angular/router';
import { EmployeeService } from 'src/app/common/service/employee_service/employee.service';
import { PopupService } from '../../../common/service/popup_service/popup.service';

@Component({
  selector: 'app-register-complaint',
  templateUrl: './register-complaint.component.html',
  styleUrls: ['./register-complaint.component.css']
})
export class RegisterComplaintComponent implements OnInit {
  complainForm: FormGroup;
  submitted = false;
  categories: any;
  areas: any;
  compDate: string;

  constructor(
    private formBuilder: FormBuilder,
    private categoryService: CategoryService,
    private areaService: AreaService,
    private complaintService: ComplaintService,
    private employeeService: EmployeeService,
    private datePipe: DatePipe,
    private loginStateService: LoginStateService,
    private router: Router,
    private popupService: PopupService
  ) {
    if (this.loginStateService.isLoginStateValidOfUser('User')) {
      this.router.navigate(['login']);
      return;
    }
  }

  ngOnInit() {
    this.compDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    this.complainForm = this.formBuilder.group({
      catId: ['', Validators.required],
      subject: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(50)]],
      shortDescription: ['', [Validators.required, Validators.minLength(50), Validators.maxLength(255)]],
      description: ['', [Validators.required, Validators.minLength(255)]],
      areaId: ['', Validators.required],
    });
    this.categories = this.categoryService.getAllActiveCategory();
    this.areas = this.areaService.getAllActiveAreas();
  }

  get f() {
    return this.complainForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.complainForm.invalid) {
      return;
    }
    const complain: ComplainModel = {
      compId: this.complaintService.getLastCompId() + 1,
      catId: +this.f.catId.value,
      userId: this.loginStateService.loginState.id,
      empId: this.employeeService.assignComplainToEmployee(),
      subject: this.f.subject.value,
      shortDescription: this.f.shortDescription.value,
      areaId: +this.f.areaId.value,
      description: this.f.description.value,
      compDate: this.compDate,
      completeDate: '',
      status: 'In progress'
    };
    this.complaintService.registerNewComplain(complain);
    this.popupService.setSuccessMessage('Your complaint is registered successfully');
    this.popupService.activateVisibleState();
    this.router.navigate(['/user']);
  }

  onCancel() {
    this.router.navigate(['/user']);
  }
}
