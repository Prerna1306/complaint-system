import { Component, OnInit } from '@angular/core';
import { LoginStateService } from 'src/app/common/service/login_state/loginstate.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-past-complaint-detail',
  templateUrl: './past-complaint-detail.component.html',
  styleUrls: ['./past-complaint-detail.component.css']
})
export class PastComplaintDetailComponent implements OnInit {


  constructor(private loginStateService: LoginStateService, private router: Router) {
    if (this.loginStateService.isLoginStateValidOfUser('User')) {
      this.router.navigate(['login']);
      return;
    }

  }

  ngOnInit() {

  }

}
