import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastComplaintDetailComponent } from './past-complaint-detail.component';

describe('PastComplaintDetailComponent', () => {
  let component: PastComplaintDetailComponent;
  let fixture: ComponentFixture<PastComplaintDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastComplaintDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastComplaintDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
