import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './component/user/user.component';
import { FormsModule } from '@angular/forms';
import { from } from 'rxjs';
import { LoginComponent } from '../login/component/login/login.component';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './component/signup/signup.component';
import { RegisterComplaintComponent } from './component/register-complaint/register-complaint.component';
import { ViewComplaintComponent } from '../complaint/component/view-complaint/view-complaint.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'newcomplaint', component: RegisterComplaintComponent }
];

@NgModule({
  declarations: [
    UserComponent,
    ViewComplaintComponent
  ],
  imports: [CommonModule, FormsModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class UserModule { }
